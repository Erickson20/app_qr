<?php

use setasign\Fpdi\Fpdi;
use setasign\Fpdi\PdfReader;
require_once('vendor/autoload.php');
require_once('config.php');
require_once('lib/nusoap.php');
require('fpdf.php');


function UploadFile($doc,$ruta,$title,$config ){
   

    $client = new nusoap_client('https://'.$config::HOSTSE.'/softexpert/webserviceproxy/se/ws/dc_ws.php',false);

    $client->setCredentials($config::USERWS,$config::PASSWS);
    $nombre = str_replace(' ','_',$title);
   
    $b64Doc = chunk_split(base64_encode(file_get_contents($ruta))); 
    
    $items = array('NMFILE'=>eliminar_tildes($nombre).'.pdf','BINFILE'=> $b64Doc, 'ERROR'=>'');
    $file = array('item'=>$items);
    $params = array('IDDOCUMENT' => $doc,'IDREVISION'=>'00','IDUSER' =>$config::USERWS,'FILE'=>$file);
    
    $response = $client->call('uploadEletronicFile',$params);
    
    //print_r($client);


    return $response ;

}

function downloadFile($doc,$config)
{
    $client = new nusoap_client('https://'.$config::HOSTSE.'/softexpert/webserviceproxy/se/ws/dc_ws.php',false);
    $client->setCredentials($config::USERWS,$config::PASSWS);
    $params = array('IDDOCUMENT' => $doc,'IDREVISION'=>'00','IDUSER'=>$config::USERWS);
    $response = $client->call('downloadEletronicFile',$params);
   
    return $response;
}


function eliminar_tildes($cadena){
 
    //Codificamos la cadena en formato utf8 en caso de que nos de errores
    //$cadena = utf8_encode($cadena);
 
    //Ahora reemplazamos las letras
    $cadena = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $cadena
    );
 
    $cadena = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $cadena );
 
    $cadena = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $cadena );
 
    $cadena = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $cadena );
 
    $cadena = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $cadena );
 
    $cadena = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C'),
        $cadena
    );
 
    return $cadena;
}

function insertQR($path,$marginleft,$marginTop,$size)
{
    # code...
}

function qrCode($params)
{
    # code...
}







?>