<?php

use setasign\Fpdi\Fpdi;
use setasign\Fpdi\PdfReader;
require_once('vendor/autoload.php');
require('fpdf.php');


$pdf = new Fpdi();
// add a page


$pdf->setSourceFile($_POST['rute']);

$pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);

$pdf->addPage();
$pdf->useImportedPage($pageId, 0, 5, 210);
$pdf->SetFont('Arial', '', '13'); 
$pdf->Image('http://chart.apis.google.com/chart?cht=qr&chs=300x300&chl='.$_POST['params'],convertX($_POST['x']), convertY($_POST['y']),size($_POST['size']),0,'PNG');
$pdf->Output();


function convertX($x)
{
    $result = ($x/2)/12.5;
    return $result;
}

function convertY($y)
{
    $result = ($y/2)/11.7;
    return $result;
}
function size($size)
{
    $result = ($size/2)/11.7;
    return $result;
}
?>