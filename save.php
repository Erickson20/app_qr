<?php

require_once('helpers.php');
$config = new Config();


$doc = $_GET['doc'];
$title = explode(".",$_GET['title']);
$rute = 'Certificaciones/'.$title[0];
$response = explode(":",UploadFile($doc,$rute,$title[0].'_QR',$config ));
print_r($title = $_GET['title']);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.7/css/mdb.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <title>Guardar datos</title>
</head>
<body style="background-color: #eceff1;">
    <div class="container">
    <br/><br/><br/>
    <?php if(strstr($response[0],'1')) {?>
        <div class="row">   
            <div class="col-md-12">
            <div class="card">
            <br/>
                <center>
                    <i class="fas fa-check-circle fa-10x  green-text"></i><br/><br/>
                    <h3><?php echo utf8_encode($response[1])?></h3>
                    <h4>ID: <?php echo $_GET['doc'];?> || NOMBRE: <?php echo $title[0].'_QR.pdf';?> </h4>
                </center>
                <br/>
            </div>
            </div>
        </div>
    <?php } else{ ?>
        <div class="row">   
            <div class="col-md-12">
            <a href="javascript:history.go(-1)" > <i class="fas fa-arrow-left fa-2x blue-text"></i> </a>
            <br/><br/>
            <div class="card">
            <br/>
                <center>
                    <i class="fas fa-times-circle fa-10x red-text"></i><br/><br/>
                    <h3><?php echo utf8_encode($response[1])?></h3>
                </center>
                <br/>
            </div>
            </div>
        </div>
    <?php }  ?>
    </div>
    
</body>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.7/js/mdb.min.js"></script>

</html>