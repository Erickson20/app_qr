<?php
require_once('helpers.php');
$doc = 'CAIDDS00000054';



$config = new Config();

$pdf_base64 = downloadFile($_GET['doc'],$config);
$content = base64_decode($pdf_base64['item']['BINFILE']);
$file = fopen('Certificaciones/'.$pdf_base64['item']['NMFILE'], "wb");
fwrite($file, $content);
fclose($file);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.7/css/mdb.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <title>Insert QR</title>
</head>
<body style="background-color: #eceff1;overflow:hidden;margin:0">
        <?php if($_GET['doc'] && $_GET['solicitud']){?>
        <div class="container" style="padding: 10px;">
        <div class="row">
        
                <div class="col-md-6 ">
                    <div class="card" style="height: 700px;overflow:hidden;margin:0;pointer-events: none;" >
                    
                    <object width="550" height="700" type="application/pdf" data="Certificaciones/<?php echo $pdf_base64['item']['NMFILE']?>?#zoom=64&scrollbar=0&toolbar=0&navpanes=0">
    
                    </object>
                    <img  id="qr" style="position: absolute;left: 3%;top: 1%;" width="120" src="http://chart.apis.google.com/chart?cht=qr&chs=300x300&chl=params"/>
                    </div>
                    
                </div>
                
                <div class="col-md-6"> 
                <form action="info.php" method="post">
                <center><h1>Generar QR</h1></center><br/>
                    <div class="row">
                        <div class="col-md-4">      
                        <label  for="12">mover X</label>
                        <input  name="x" onkeypress="return keyX(event)" onkeydown="moveX()" class="form-control" id="x" type="number" required/>
                        </div>
                       
                        <div class="col-md-4">   
                        <label  for="12">mover Y</label>   
                        <input  name="y" onkeydown="moveY()" class="form-control" id="y" type="number" required/>
                        </div>

                        <div class="col-md-4">   
                        <label  for="12">size</label>   
                        <input  name="size" onkeydown="sizeQr()" class="form-control" id="size" type="number"required/>
                        <input  name="rute"  value="Certificaciones/<?php echo $pdf_base64['item']['NMFILE']; ?>" class="form-control" id="rute" type="text"hidden/>
                        </div>
                        
                    </div> 
                    <br/>
                    <label  for="12">No. solicitud</label> 
                    <input readonly class="form-control" value="<?php echo  $_GET['solicitud']; ?>" name="params" type="text" required/>
                    <br/>
                    <center>
                   <a href="save.php?doc=<?php echo $_GET['doc']?>&title=<?php echo $pdf_base64['item']['NMFILE']; ?>"  class="btn btn-primary ">Guardar en softexpert</a>
                   <button  type="submit"class="btn btn-success ">Ver PDF</button>
                   </center>
                   </form>
                </div>
            </div>
        </div>
        <?php }else{ ?>
        <center><h1>Los parametros son invalidos :(</h1></center>
        <?php } ?>
 
</body>
<script>
       function moveX() {
           let x = document.getElementById('x').value;
           let qr = document.getElementById('qr');
           qr.style.marginLeft = x+"px"
           console.log('cod :', x);
       }
       function moveY() {
           let y = document.getElementById('y').value;
           let qr = document.getElementById('qr');
           qr.style.marginTop = y+"px"
           console.log('cod :', y);

        }
        function sizeQr() {
           let size = document.getElementById('size').value;
           let qr = document.getElementById('qr');
          qr.width = size
           console.log('cod :', size);

        }

        //<img  id="qr" width="120" src="http://chart.apis.google.com/chart?cht=qr&chs=300x300&chl=params"/>
        function keyX(event) {
            var x = document.getElementById('x').value
            console.log('event.value :', event);
            if(x<4000){
                return true
            }
            return false
            
        }
</script>
<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.7/js/mdb.min.js"></script>
</html>